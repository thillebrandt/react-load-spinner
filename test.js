"use strict";

console.time("Test");

import React from 'react';
import ReactDOM from 'react-dom';

import LoadSpinner from './src/load-spinner';

const app = (
	<LoadSpinner className="spin" count={25} />
);

console.log("Uhm..");

ReactDOM.render(
	app,
	document.getElementById('app-element'),
	function() {
		// Callback, run after the app is rendered.
		console.log("Test loaded");
		console.timeEnd("Test");
	}
);
