# react-load-spinner

Small library of "spinner" components to add to any ReactJS page to indicate loading.



## Installation

- `npm install --save-dev https://Palelion@bitbucket.org/Palelion/react-load-spinner.git`


## Instructions

`import LoadSpinner from 'react-load-spinner';`  
  
  

## Arguments

### style
  - Which style of "spinner" to display:
    "classic" or "petals": The well-known radial spinner.
    "squares":             A matrix of squares flashing in random order.
    "bars":                A row of bars flowing from the middle and outward.
    "spinner":             Another classic; gradient spinning circle.
    "spiral":              A variation of "petals", forming an inside or outside spiral.

### count
  - The number of elements in the selected spinner.
    "petals" and "spiral": Defines the amount of petals.
    "squares":             Defines the number of blocks in the matrix.
    "bars":	               Defines the number of bars in the row.
    "spinner":             Defines roughly the amount of the circle will be filled.

### speed
  - The speed of the animation.

### width / height
  - The width or height in pixels of the spinner itself.

### size
  - The width _and_ height in pixels of the spinner itself.

### boxWidth / boxHeight
  - The width or height in pixels of the container for the spinner; cannot be smaller than
    the size of the spinner itself.

### spiral
  - Only applies to "spiral" type. 1-4, the style of the spiral (inward or outward, flat or
    pointed.)
