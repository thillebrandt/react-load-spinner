import React from 'react';
import ExtractCSS from 'js-extract-css';

const DEFAULTS = {
	type:				'classic',
	className:	undefined,
	width:			undefined,
	height:			undefined,
	boxWidth:		undefined,
	boxHeight:	undefined,
	speed:			10,
	count:			undefined,
	spiral:			1
};

export default function LoadSpinner(props) {
	props = Object.assign({}, DEFAULTS, props);

// props:
// style: which spinner type; gotta come up with a few
// className: custom CSS class; use this to set the general color
// width: size in px of the spinner
// height: height in px of the spinner; shouldn't be used, unless you want eliptical spinner
// boxWidth: width of the bounding box; for when the spinner should take up more space than its size
// boxHeight: height of the bounding box
// speed: how fast should it spin; negative speed means it'll spin backwards
// count: number of elements; i.e. for the spinner types it's petals

	if (!props.color || !props.width || !props.boxWidth) {
		ExtractCSS({class: props.className, element: 'svg', feather: true, firstOnly: true}, (cssDetails) => {

			if (!props.color) props.color = cssDetails.styles ? cssDetails.styles.color : 'rgb(0,0,0)';
			if (!props.width) props.width = cssDetails.styles ? cssDetails.styles.width : 64;
			if (!props.boxWidth) props.boxWidth = cssDetails.styles ? cssDetails.styles.width : undefined;
			if (!props.boxHeight) props.boxHeight = cssDetails.styles ? cssDetails.styles.height : undefined;
		});
	}

	if (props.size) props.width = props.height = props.size;

	props.boxWidth = props.boxWidth || props.width;
	props.boxHeight = props.boxHeight || props.height || props.width;

	if (props.boxWidth < props.width) props.boxWidth = props.width;
	if (props.height && props.boxHeight && props.boxHeight < props.height) props.boxHeight = props.height;

	if (!props.height) props.height = props.width;
	if (!props.boxHeight) props.boxHeight = props.boxWidth;

	if (Number.isInteger(props.width)) props.width = `${props.width}px`;
	if (Number.isInteger(props.height)) props.height = `${props.height}px`;
	if (Number.isInteger(props.boxWidth)) props.boxWidth = `${props.boxWidth}px`;
	if (Number.isInteger(props.boxHeight)) props.boxHeight = `${props.boxHeight}px`;

	try {
		return (
			<div className={props.className} style={{textAlign: 'center', minWidth: `${props.boxWidth}`, width: `${props.boxWidth}`, minHeight: `${props.boxHeight || props.boxWidth}`, height: `${props.boxHeight || props.boxWidth}`}}>
				<svg	xmlns="http://www.w3.org/2000/svg"
							width={props.width} height={props.height || props.width}
							viewBox="0 0 200 200"
							style={{paddingTop: `${props.boxHeight / 2 - ((props.height || props.width) / 2)}`}}
							className={props.className}
							>
					{DoStyle(props)}
				</svg>
			</div>
		);
	}
	catch(e) {
		console.error(`FAILURE!!!  ${e.fileName}\n${e.message}`);
		return(
			<span />
		);
	}
}

function DoStyle(props) {
	switch(props.type.toLowerCase()) {
	case "squares":
		return Squares(props.count, props.speed, props.className, props.color);
	case "bars":
		return Bars(props.count, props.speed, props.className, props.color);
	case "spinner":
		return Spinner(props.count, props.speed, props.className, props.color);
	case "spiral":
		return Petals(props.count, props.speed, props.className, props.color, props.spiral);
	case "classic":
	case "petals":
	default:
		return Petals(props.count, props.speed, props.className, props.color, false);
	}
}

/*
function GetColor(className) {
	try {
		const sheets = Array.from(document.styleSheets);
		const colorPriority = [undefined, undefined, undefined];

		sheets.map((sheet) => {
			Array.from(sheet.cssRules).map((rule) => {
				if (rule.selectorText) {
					const selectors = rule.selectorText.split(' ');
					selectors.map((selector) => {
	*///					const real = /(.*)\.([-a-z]*),*:*/.exec(selector);
		/*				if (real) {
							if (real[2] === className) {
								if (real[1].toLowerCase() === "svg") {
									colorPriority[0] = rule.style.color;
								} else if (real[1] === "") {
									colorPriority[1] = rule.style.color;
								} else {
									colorPriority[2] = rule.style.color;
								}
							}
						}
					});
				}
			});
		});
		return colorPriority[0] || colorPriority[1] || colorPriority[2] || '#000000';
	}
	catch(e) {
		console.error(e.message);
		return undefined;
	}
}
*/

function Petals(petalCount, givenSpeed, className, color, spiral) { // "classic", "petals", or "spiral"
	const finalShape = [];
	const baseSpeed = 10.0 / givenSpeed;
	if (!petalCount) petalCount = 15;

	let petalSpacer = 0.2 / (petalCount / 10);
	if (spiral) {
		petalSpacer = ((2 * Math.PI) / petalCount) / 2;
	}

	for (let petalNumber = 1; petalNumber <= petalCount; petalNumber++) {
		const PI = ((2 * Math.PI) / petalCount) * petalNumber;

		const xaa = 100 + (100 * Math.cos(PI - petalSpacer));
		const yaa = 100 + (100 * Math.sin(PI - petalSpacer));
		const xba = 100 + (100 * Math.cos(PI + petalSpacer));
		const yba = 100 + (100 * Math.sin(PI + petalSpacer));
		const xbb = 100 + (spiral ? 12 : 30 * Math.cos(PI - petalSpacer));
		const ybb = 100 + (spiral ? 12 : 30 * Math.sin(PI - petalSpacer));
		const xab = 100 + (spiral ? 12 : 30 * Math.cos(PI + petalSpacer));
		const yab = 100 + (spiral ? 12 : 30 * Math.sin(PI + petalSpacer));

		const spirals = [null,
			`${xaa}, ${yaa} ${xba}, ${yba} ${xba}, ${yba} ${xaa}, ${yaa}`,	// flat top
			`${xbb}, ${ybb} ${xab}, ${yab} ${xab}, ${yab} ${xbb}, ${ybb}`,	// flat bottom
			`${xaa}, ${yaa} ${xaa}, ${yaa} ${xaa}, ${yaa} ${xaa}, ${yaa}`,	// point top
			`${xbb}, ${ybb} ${xbb}, ${ybb} ${xbb}, ${ybb} ${xbb}, ${ybb}`		// point bottom
		];

		if (spiral >= spirals.length) spiral = spirals.length - 1;

		finalShape.push(
			<polygon	key={`pg${petalNumber}`} opacity={0.0} fill={color}
								points={`${xaa}, ${yaa} ${xba}, ${yba} ${xab}, ${yab} ${xbb}, ${ybb}`}
								className={className}
								>
				<animate attributeType='XML' attributeName='opacity' from='1.0' to='0.1'
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / petalCount) * petalNumber}s`} />
				{spiral ?
				<animate attributeType='XML' attributeName='points'
					from={`${xaa}, ${yaa} ${xba}, ${yba} ${xab}, ${yab} ${xbb}, ${ybb}`}
					to={spirals[spiral]}
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / petalCount) * petalNumber}s`} />
				: <span />}
			</polygon>
		);
	}

	return finalShape;
}


function Squares(squareCount, givenSpeed, className, color) { // "squares"
	const finalShape = [];
	const baseSpeed = 20.0 / givenSpeed;
	if (!squareCount) squareCount = 4;

	//find factor
	let factor = Math.floor(Math.sqrt(squareCount));
	while (Math.ceil(squareCount / factor) != (squareCount / factor)) {
		factor--;
	}

	const squareArray = squareCount / factor;
	const squareDimension = 200 / squareArray;
	const zoomStart	= 1.0;
	const zoomEnd		= 0.9;

	let squareX = 0, squareY = 0;
	const squares = [];

	for (let squareNumber = 0; squareNumber < squareCount; squareNumber++) {
		if (squareX >= squareArray) {
			squareX = 0;
			squareY++;
		}
		squares.push([squareX, squareY]);
		squareX++;
	}

	for (let squareNumber = 0; squareNumber < squareCount; squareNumber++) {
		const squareCoords = squares.splice(Math.floor(Math.random() * squares.length), 1)[0];
		squareX = squareCoords[0]; squareY = squareCoords[1];

		const posX = (squareX * squareDimension) + ((squareDimension * (1.0 - zoomEnd)) / squareArray);
		const posY = (squareY * squareDimension) + ((squareDimension * (1.0 - zoomEnd)) / squareArray) + (((squareDimension * squareArray) - (squareDimension * (squareCount / squareArray))) / 2);

		finalShape.push(
			<rect	key={`rc${squareNumber}`} opacity={0.0} fill={color}
						x={posX} y={posY} width={squareDimension * zoomEnd} height={squareDimension * zoomEnd}
						className={className}
						>
				<animate attributeType='XML' attributeName='opacity' from='1.0' to='0.1'
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / squareCount) * squareNumber}s`} />
				<animate attributeType='XML' attributeName='width' from={squareDimension * zoomStart} to={squareDimension * zoomEnd}
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / squareCount) * squareNumber}s`} />
				<animate attributeType='XML' attributeName='x' from={posX - (((squareDimension * zoomStart) - (squareDimension * zoomEnd)) / 2)} to={posX}
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / squareCount) * squareNumber}s`} />
				<animate attributeType='XML' attributeName='height' from={squareDimension * zoomStart} to={squareDimension * zoomEnd}
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / squareCount) * squareNumber}s`} />
				<animate attributeType='XML' attributeName='y' from={posY - (((squareDimension * zoomStart) - (squareDimension * zoomEnd)) / 2)} to={posY}
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / squareCount) * squareNumber}s`} />
			</rect>
		);
	}

	return finalShape;
}

function Bars(barCount, givenSpeed, className, color) { // "bars"
	const finalShape = [];
	const baseSpeed = 9.0 / givenSpeed;

	if (!barCount) barCount = 3;
	if (barCount / 2 == Math.floor(barCount / 2)) barCount += 1;

	for (let barNumber = 0; barNumber < Math.ceil(barCount / 2); barNumber++) {
		let barX = ((200 / barCount) * barNumber) + (Math.floor(barCount / 2) * (200 / barCount));
		finalShape.push(
			<rect	key={`br${barNumber}`} opacity={0.0} fill={color}
						x={barX} y={0} width={200 / barCount * 0.9} height={200}
						className={className}
						>
				<animate attributeType='XML' attributeName='opacity' from='1.0' to='0.1'
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / Math.ceil(barCount / 2)) * barNumber}s`} />
				<animate attributeType='XML' attributeName='height' from={200} to={100}
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / Math.ceil(barCount / 2)) * barNumber}s`} />
				<animate attributeType='XML' attributeName='y' from={0} to={50}
					dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / Math.ceil(barCount / 2)) * barNumber}s`} />
			</rect>
		);

		if (barNumber != 0) {
			barX = ((-200 / barCount) * (barNumber + 1)) + (Math.floor(barCount / -2) * (-200 / barCount));
			finalShape.push(
				<rect	key={`bl${barNumber}`} opacity={0.0} fill={color}
							x={barX} y={0} width={200 / barCount * 0.9} height={200}
							className={className}
							>
					<animate attributeType='XML' attributeName='opacity' from='1.0' to='0.1'
						dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / Math.ceil(barCount / 2)) * barNumber}s`} />
					<animate attributeType='XML' attributeName='height' from={200} to={100}
						dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / Math.ceil(barCount / 2)) * barNumber}s`} />
					<animate attributeType='XML' attributeName='y' from={0} to={50}
						dur={`${baseSpeed}s`} repeatCount='indefinite' begin={`${(baseSpeed / Math.ceil(barCount / 2)) * barNumber}s`} />
				</rect>
			);
		}
	}

	return finalShape;
}

function Spinner(spinnerSize, givenSpeed, className, color) { // "spinner"
	const finalShape = [];
	const baseSpeed = 10.0 / givenSpeed;
	const spinnerPercent = 15 * (spinnerSize || 3);

	finalShape.push(
		<defs	key={`sp_defs`}>
			<linearGradient id="circleGradient">
				<stop offset={`${0}%`} stopOpacity="1.0" stopColor={color} />
				<stop offset={`${spinnerPercent}%`} stopOpacity="0.0" stopColor={color} />
			</linearGradient>
		</defs>,
		<circle	key={`sp_inner`} className={className}
						cx={100} cy={100} r={84}
						strokeWidth={2} stroke={color} strokeOpacity={0.5} fill={`none`}
						/>,
		<circle	key={`sp_spinner`} className={className}
						cx={100} cy={100} r={91}
						strokeWidth={10} stroke={`url(#circleGradient)`} fill={`none`}
						>
			<animateTransform attributeType='XML' attributeName='transform' type='rotate'
						from='0 100 100' to='360 100 100'
						dur={`${baseSpeed}s`} repeatCount='indefinite' />
		</circle>,
		<circle	key={`sp_outer`} className={className}
						cx={100} cy={100} r={98}
						strokeWidth={2} stroke={color} strokeOpacity={0.5} fill={color} fillOpacity={0.05}
						/>
	);


	return finalShape;
}
